package main

import (
	"fmt"
)

func main() {
	fmt.Println(Add5(10))
}

// Add5 returns the input incremented by 5
func Add5(n int) int {
	return n + 5
}
